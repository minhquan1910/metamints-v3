const MAX_HEIGHT = 8;
class Account {
    constructor(directReferrer, leftHeight, rightHeight, leftBonus, rightBonus, maxVolume) {
        this.directReferrer = directReferrer;
        this.leftHeight = leftHeight;
        this.rightHeight = rightHeight;
        this.leftBonus = leftBonus;
        this.rightBonus = rightBonus;
        this.maxVolume = maxVolume;
    }
    getAccount() {
        return {
            directReferrer: this.directReferrer,
            leftHeight: this.leftHeight,
            rightHeight: this.rightHeight,
            leftBonus: this.leftBonus,
            rightBonus: this.rightBonus,
            maxVolume: this.maxVolume,
        };
    }
    isOverMaxHeight() {
        return this.leftHeight >= MAX_HEIGHT || this.rightHeight >= MAX_HEIGHT;
    }
}

module.exports = Account;