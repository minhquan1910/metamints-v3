
const DatabaseImpl = require("../database");
const wrapper = require("../helpers/wrapper");
const accountServices = require("../services/accountServices");

const getAccountInfo = async (event, context) => {
    try {
        console.log("Body: ", event?.body);
        const {
            address,
        } = JSON.parse(event?.body);
        if (!address) {
            throw new Error("Address is required");
        }
        // query db
        const account = await accountServices.getAccountByAddress(address);
        return account;
    } catch (err) {
        const error = { err };
        console.error("Error: ", error);
        const reason = error?.err?.reason?.toString() ?? error?.err?.message?.toString();
        console.error("Reason: ", reason);
        return new Promise((resolve, reject) => {
            reject(reason);
        });
    }
}
exports.handler = async function (event, context) {
    const db = new DatabaseImpl();
    try {
        await db.connect();
        const result = await getAccountInfo(event, context);
        await db.close();
        return wrapper(200, result);
    } catch (error) {
        await db.close();
        console.error(error);
        return wrapper(500, { error: error });
    }
}