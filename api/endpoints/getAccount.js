const DatabaseImpl = require("../database");
const accountServices = require("../services/accountServices");
const wrapper = require("../helpers/wrapper");

const getAccount = async (event, context) => {
    try {
        console.log("Body: ", event.body);
        const {
            address,
        } = JSON.parse(event.body);
        if (!address) {
            throw new Error("Address is required");
        }
        const account = await accountServices.getAccountByAddress(address);
        console.log("Account: ", account);
        if (account && account?.isCloned === false) {
            const isCloned = await accountServices.checkCloned(account.contractOwner);
            console.log("Is cloned: ", isCloned);
            if (isCloned) {
                await accountServices.updateOne(address, { isCloned: true });
            }
            return {
                ...account,
                isCloned
            }
        }
        return account;
    } catch (error) {
        return new Promise((resolve, reject) => {
            reject(error);
        });
    }
}
exports.handler = async function (event, context) {
    const db = new DatabaseImpl();
    try {
        await db.connect();
        const result = await getAccount(event, context);
        await db.close();
        return wrapper(200, result);
    } catch (error) {
        await db.close();
        console.error(error);
        return wrapper(500, { error: error.message });
    }
}