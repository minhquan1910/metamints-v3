const { isEmpty } = require("lodash");
const DatabaseImpl = require("../database");
const binaryPlan = require("../contracts/binaryPlan");
const wrapper = require("../helpers/wrapper");
const accountServices = require("../services/accountServices");

const updateVolume = async (event, context) => {
    try {
        console.log("Body: ", event?.body);
        const {
            address,
            volume
        } = JSON.parse(event?.body);
        if (!address) {
            throw new Error("Address is required");
        }
        const account = await accountServices.getAccountByAddress(address);
        if (!isEmpty(account)) {
            const binaryPlanContract = new binaryPlan(account?.contract);
            await binaryPlanContract.wid(address, volume);
            return {
                updatedVolume: "success"
            }
        }
        else {
            throw new Error("Account not found");
        }

    } catch (err) {
        const error = { err };
        console.error("Error: ", error);
        const reason = error?.err?.reason?.toString() ?? error?.err?.message?.toString();
        console.error("Reason: ", reason);
        return new Promise((resolve, reject) => {
            reject(reason);
        });
    }
}
exports.handler = async function (event, context) {
    const db = new DatabaseImpl();
    try {
        await db.connect();
        const result = await updateVolume(event, context);
        await db.close();
        return wrapper(200, result);
    } catch (error) {
        await db.close();
        console.error(error);
        return wrapper(500, { error: error });
    }
}