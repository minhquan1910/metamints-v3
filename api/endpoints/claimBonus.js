
const { isEmpty } = require("lodash");
const DatabaseImpl = require("../database");
const binaryPlan = require("../contracts/binaryPlan");
const Treasury = require("../contracts/treasury");
const wrapper = require("../helpers/wrapper");
const accountServices = require("../services/accountServices");
const constants = require("../constants");

const claimBonus = async (event, context) => {
    try {
        console.log("Body: ", event?.body);
        const {
            address,
            signature,

        } = JSON.parse(event?.body);
        if (!address) {
            throw new Error("Address is required");
        }
        const account = await accountServices.getAccountByAddress(address);
        if (!isEmpty(account)) {
            const binaryPlanContract = new binaryPlan(account?.contract);
            const bonus = await binaryPlanContract.withdrawableAmt(address);
            console.log("Bonus: ", bonus);
            const block = await constants.provider.getBlock("latest")
            const timestamp = block.timestamp
            console.log("Timestamp ", timestamp)
            const deadline = (timestamp + 900).toString()
            await Treasury.withdraw(address, bonus, signature, deadline);
        }
        else {
            throw new Error("Account not found");
        }

    } catch (err) {
        const error = { err };
        console.error("Error: ", error);
        const reason = error?.err?.reason?.toString() ?? error?.err?.message?.toString();
        console.error("Reason: ", reason);
        return new Promise((resolve, reject) => {
            reject(reason);
        });
    }
}
exports.handler = async function (event, context) {
    const db = new DatabaseImpl();
    try {
        await db.connect();
        const result = await claimBonus(event, context);
        await db.close();
        return wrapper(200, result);
    } catch (error) {
        await db.close();
        console.error(error);
        return wrapper(500, { error: error });
    }
}