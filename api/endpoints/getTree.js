const binaryPlan = require("../contracts/binaryPlan");
const wrapper = require("../helpers/wrapper");
const Factory = require('../contracts/factory');
const getTree = async (event, context) => {
    try {
        console.log("Body: ", event.body);
        const {
            address,
            contract,
            contractOwner,
            isCloned
        } = JSON.parse(event.body);
        if (!address) {
            throw new Error("Address is required");
        }
        let trees = []
        if (isCloned === true) {
            const binaryContract = new binaryPlan(contract);
            trees = await binaryContract.getTree(address);
            console.log("Tx: ", trees);
        }
        else {
            const factory = new Factory();
            const result = await factory.cloneOf(contractOwner);
            console.log("Result: ", result);
            if(result && result.isCloned === true){
                const binaryContract = new binaryPlan(result?.contract);
                trees = await binaryContract.getTree(address);
            }
        }
        return trees;
    } catch (error) {
        return new Promise((resolve, reject) => {
            reject(error);
        });
    }
}
exports.handler = async function (event, context) {
    try {
        const result = await getTree(event, context);
        return wrapper(200, result);
    } catch (error) {
        console.error(error);
        return wrapper(500, { error: error.message });
    }
}