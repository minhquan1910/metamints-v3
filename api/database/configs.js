const configs = {
    mongoDb: {
        DB_NAME: "metamint",
        configs: {
            PASSWORD: process.env.MONGODB_PASSWORD,
            USERNAME: process.env.MONGODB_USERNAME,
            CLUSTER: process.env.MONGODB_CLUSTER,
        }
    },
}
module.exports = configs;