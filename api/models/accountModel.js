var mongoose = require("mongoose");
var constants = require("../constants");
const Account = new mongoose.Schema({
    address: String,
    code: String,
    referrer: String,
    isLeft: Boolean,
    contract: String,
    contractOwner: String,
    isCloned: Boolean,
}, {
    timestamps: true
});
const DBAccountModel = mongoose.model(constants.mongoDb.collectionName.ACCOUNT, Account);
module.exports = DBAccountModel;