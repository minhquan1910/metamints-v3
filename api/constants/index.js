var marketplaceAddress = require('./contract/marketplace').marketplaceAddress;
var marketplaceABI = require('./contract/marketplace').marketplaceABI;
var { binaryPlanABI } = require('./contract/binaryplan');
var { FactoryABI, FactoryAddress } = require('./contract/factory');
var { AuthorityABI, AuthorityAddress } = require('./contract/authority');
var { TreasuryABI, TreasuryAddress } = require('./contract/treasury');
var { BUSD } = require('./tokens');
var provider = require('./provider');
const constants = {
    contracts: {
        TREASURY_ADDRESS: TreasuryAddress,
        TREASURY_ABI: JSON.parse(TreasuryABI),
        AUTHORITY_ADDRESS: AuthorityAddress,
        AUTHORITY_ABI: JSON.parse(AuthorityABI),
        MARKETPLACE_ADDRESS: marketplaceAddress,
        MARKETPLACE_ABI: marketplaceABI,
        BINARY_PLAN_ABI: JSON.parse(binaryPlanABI),
        FACTORY_ADDRESS: FactoryAddress,
        FACTORY_ABI: JSON.parse(FactoryABI)
    },
    tokens: { BUSD },
    mongoDb: {
        collectionName: {
            ACCOUNT: "accounts",
        },
    },
    DEFAULT_LENGTH_CODE: 6,
    ADDRESS_DEFAULT_REFERRAL: "0x0000000000000000000000000000000000000000",
    provider
}
module.exports = constants