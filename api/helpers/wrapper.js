module.exports = async (statusCode, result) => {
    console.log("Wrapper - result: ",result);
    const response = {
        statusCode,
        headers: {
            'Access-Control-Allow-Origin': '*',
            'Access-Control-Allow-Headers': 'Content-Type',
            'Access-Control-Allow-Methods': 'GET, POST, PUT, PATCH, DELETE',
            'Access-Control-Allow-Credentials': true,
        },
        body: JSON.stringify({
            code: statusCode,
            data: result
        }),
    };
    return response;
}