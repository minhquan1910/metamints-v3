const { ethers } = require("ethers");
const { isEmpty } = require("lodash");
const constants = require("../constants");

class Factory {
    constructor() {
        const signer = new ethers.Wallet(process.env.PRIVATE_KEY_OWNER, constants.provider);
        this._contract = new ethers.Contract(constants.contracts.FACTORY_ADDRESS, constants.contracts.FACTORY_ABI, signer);
    }
    clone = async (address) => {
        console.log("=== Clone ===");
        console.log("Input", address);
        const tx = await this._contract.clone(address);
        console.log("Tx hash: ", tx.hash);
        // await tx.wait();
        const result = await this.cloneOf(address)
        return result;
    }
    cloneOf = async (address) => {
        console.log("==== CloneOf ====");
        console.log("Input", address)
        const contract = await this._contract.cloneOf(address) 
        console.log("=== Contract address: ", contract);
        let contractAddress = '';
        let isCloned = true;
        if (contract?.length >= 2) {
            if (!isEmpty(contract[0])) {
                contractAddress = contract[0]
                isCloned = contract[1]
            }
        }
        return {
            contract: contractAddress,
            isCloned,
        };
    }
}
module.exports = Factory;