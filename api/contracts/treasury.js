const { ethers } = require("ethers");
const constants = require("../constants");

class TreasuryServices {
    constructor() {
        this.treasury = new ethers.Contract(constants.contracts.TREASURY_ADDRESS, constants.contracts.TREASURY_ABI, constants.provider);
    }

    async getTreasury() {
        return await this.treasury.getTreasury();
    }
    withdraw = async (address, amount, signature, deadline) => {
        console.log("===Start Withdraw ===");
        const signer = new ethers.Wallet(process.env.PRIVATE_KEY_OWNER, constants.provider);
        const contract = new ethers.Contract(constants.contracts.TREASURY_ADDRESS, constants.contracts.TREASURY_ABI, signer);
        const tx = await contract.withdraw(constants.tokens.BUSD.address, address, amount, deadline , signature);
        console.log("Tx hash: ", tx.hash);
        console.log(" =========End Withdraw========== ");
        return tx;
    }
}
module.exports = new TreasuryServices();