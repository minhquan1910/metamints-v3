const { ethers } = require("ethers");
const { isEmpty } = require("lodash");
const constants = require("../constants");
const Account = require("../repositories/account");
class BinaryPlanServices {
    constructor(address) {
        console.log("=== Constructor BinaryPlanServices ===");
        console.log("Address contract", address);
        const signer = new ethers.Wallet(process.env.PRIVATE_KEY_OWNER, constants.provider);
        this._contract = new ethers.Contract(address, constants.contracts.BINARY_PLAN_ABI, signer);
    }
    getContract() {
        return this._contract;
    }
    getAccount = async (address) => {
        const result = await this._contract.accounts(address);
        return new Account(...result);
    }
    getTree = async (address) => {
        console.log("===Start Get tree of account ===");
        console.log("Address input", address);
        console.log("Contract", this._contract.address);
        const tree = await this._contract.getTree(address);
        console.log("Tree: ", tree);
        console.log("===End Get tree of account ===");
        return tree;
    }
    addReferrer = async (referee, referrer = constants.ADDRESS_DEFAULT_REFERRAL, isLeft = true) => {
        console.log("===Start Add referer ===");
        console.log("Input", { referee, referrer, isLeft });
        console.log("Contract", this._contract.address)
        if (isEmpty(referee) || isEmpty(referrer) || typeof isLeft !== "boolean") {
            throw new Error("Invalid params to call addReferrer function in smart contract");
        }
        const tx = await this._contract.addReferrer(
            referrer,
            referee,
            isLeft
        );
        console.log("Tx hash: ", tx.hash);
        console.log(" =========End Add Referrer========== ");
        return tx;
    }
    wid = async (address, volume) => {
        console.log("===Start Update Volume ===");
        console.log("Input", { address, volume });
        console.log("Contract", this._contract.address);
        if (isEmpty(address) || isEmpty(volume)) {
            throw new Error("Invalid params to call updateVolume function in smart contract");
        }
        const parsedVolume = ethers.utils.parseEther(volume);
        const tx = await this._contract.updateVolume(address, parsedVolume);
        console.log("Tx hash: ", tx.hash);
        console.log(" =========End Update Volume========== ");
        return tx;
    }
    withdrawableAmt = async (address) => {
        console.log("===Start Withdraw ===");
        console.log("Input", { address });
        console.log("Contract", this._contract.address);
        if (isEmpty(address)) {
            throw new Error("Invalid params to call withdraw function in smart contract");
        }
        const received = await this._contract.withdrawableAmt(address);
        console.log("Tx hash: ", received);
        console.log(" =========End Withdraw========== ");
        return received;
    }
}
module.exports = BinaryPlanServices;