const accountModel = require('../models/accountModel');
const random = require('../helpers/random');
const constants = require('../constants');
const Factory = require('../contracts/factory');
const binaryPlan = require("../contracts/binaryPlan");
const { isEmpty } = require('lodash');
class AccountDBServices {
    save = async (address, code, isLeft) => {
        console.log("=== Start Save Services ===")
        console.log("input", {
            address,
            code,
            isLeft
        })
        const existingAccount = await this.getAccountByAddress(address);
        if (!isEmpty(existingAccount)) {
            console.log(`Account ${address} already exists`);
            if (!existingAccount.isCloned) {
                const isCloned = await this.checkCloned(existingAccount.contractOwner);
                console.log("Is cloned: ", isCloned);
                await accountModel.updateOne({ address }, { isCloned });
                return {
                    ...existingAccount,
                    isCloned
                }
            }
            return existingAccount;
        }
        console.log(`Account ${address} is not existed`);
        const newAccount = {
            address: address.toLowerCase(),
            code: random(constants.DEFAULT_LENGTH_CODE),
        }
        console.log("Code", code);
        if (isEmpty(code)) {
            const factory = new Factory();
            const result = await factory.clone(address);
            newAccount.referrer = constants.ADDRESS_DEFAULT_REFERRAL.toLowerCase();
            newAccount.isLeft = false;
            newAccount.contract = result.contract;
            newAccount.contractOwner = address;
            newAccount.isCloned = result?.isCloned;
        } else {
            const referrer = await this.getAccountByCode(code);
            console.log(`Referrer of ${address}`, referrer);
            if (!referrer) {
                throw new Error("Referral code is invalid");
            }
            newAccount.referrer = referrer.address;
            newAccount.isLeft = isLeft;
            newAccount.contract = referrer.contract;
            newAccount.contractOwner = referrer.contractOwner;
            newAccount.isCloned = referrer.isCloned;
            if (!isEmpty(newAccount.contract)) {
                const { address, referrer, isLeft, contract } = newAccount;
                const binaryContract = new binaryPlan(contract);
                const tx = await binaryContract.addReferrer(address, referrer, isLeft);
                console.log("Tx: ", tx);
            }
            else {
                throw new Error("Address of binary contract must be valid")
            }
        }
        const account = (await accountModel.create(newAccount))?.toObject() || {};
        console.log("=== End Save Services ===")
        return account;
    }

    checkCloned = async (address) => {
        console.log("=== Check Cloned Services ===")
        if (!address) {
            throw new Error(`Address contract must be valid`);
        }
        const factory = new Factory();
        const result = await factory.cloneOf(address);
        return result.isCloned;
    }

    getAccountByAddress = async (address) => {
        const account = (await accountModel.findOne({ address: address }))?.toObject() || {};
        return account;
    }
    getAccountByCode = async (code) => {
        const account = (await accountModel.findOne({ code: code }))?.toObject() ?? {};
        return account;
    }
    updateOne = async (address, data) => {
        const account = await accountModel.updateOne({ address }, data);
        return account;
    }
}

module.exports = new AccountDBServices();