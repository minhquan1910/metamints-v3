import { includes } from "lodash";
const blackList= ["0x4677ef8cddfdd8c17f2ccf6dd7566543a408f40b"]
const isBlackListAddress = (account) => {
    return includes(blackList, account);
}
export default isBlackListAddress;