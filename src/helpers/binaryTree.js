import BinaryNode from "./binaryNode";
class BinaryTree {
    constructor(data) {
        this.data = data;
    }
    insertLevelOrder(i) {
        let root;
        if (i < this.data.length) {
            root = new BinaryNode(this.data[i]);
            root.setLeft(this.insertLevelOrder(2 * i + 1));
            root.setRight(this.insertLevelOrder(2 * i + 2));
        }
        return root;
    }
}
export default BinaryTree;