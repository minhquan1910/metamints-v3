import Icon from '@ant-design/icons';

import { ReactComponent as FloorPrice } from './FloorPrice.svg';

const FloorPriceIcon = (props) => {
  return <Icon component={FloorPrice} {...props} />;
};

export default FloorPriceIcon;
