// rafce
import { PlusOutlined, RightOutlined } from '@ant-design/icons'
import { Button, Input, Modal } from 'antd'
import Constants from 'constant';
import { checkWalletConnection } from 'helpers/auth';
import { failureModal, successModal } from 'helpers/modals';
import React, { useState } from 'react'
import { useMoralis } from 'react-moralis';
import styles from "../../../styles.module.css";

const ModalReferral = () => {

    const { account, authenticate, isAuthenticated } = useMoralis();
    const [data, setData] = useState({});
    const [isLoadingLink, setIsLoadingLink] = useState(false);
    const [showInput, setShowInput] = useState(false);
    const [open, setOpen] = useState(false);
    const [referralLink, setReferralLink] = useState("");
    const handleCreateLinkClick = async () => {
        await checkWalletConnection(isAuthenticated, authenticate, createLink);
    };
    const handleAddReferralClick = async () => {
        await checkWalletConnection(isAuthenticated, authenticate, addReferral);
    };
    const createLink = async () => {
        setIsLoadingLink(true);
        const { DOMAIN, SUB_DOMAIN, endpoints } = Constants.apiConfig;
        // const DOMAIN = "http://localhost:7000/";
        const saveAccountUrl = `${DOMAIN}${SUB_DOMAIN}${endpoints.SAVE_ACCOUNT}`;
        const body = {
            address: account,
        };
        const optionSaveAccount = {
            method: "POST",
            body: JSON.stringify(body),
        };
        console.log("Save Account URL", saveAccountUrl);
        console.log("Save Account Option", optionSaveAccount);
        fetch(saveAccountUrl, optionSaveAccount)
            .then((res) => res.json())
            .then((result) => {
                console.log("Data", result);
                if (result.code === 200) {
                    console.log("Account Data in create link", result.data);
                    setData({
                        ...result.data,
                        linkLeft: `${window.location.origin}/invite/left/${result.data.code}`,
                        linkRight: `${window.location.origin}/invite/right/${result.data.code}`,
                    });
                } else {
                    failureModal("Error", result?.data?.error);
                }
                setIsLoadingLink(false);
                setOpen(false);
            })
            .catch((err) => {
                failureModal("Error", "Something went wrong while saving account");
                console.error("Error", err);
                setIsLoadingLink(false);
                setOpen(false);
            });
    };
    const addReferral = async () => {
        const address = account;
        const linkParts = referralLink.split("/");
        console.log("Link Parts", linkParts);
        const code = linkParts[linkParts.length - 1];
        const side = linkParts[linkParts.length - 2];
        if (isAuthenticated && address) {
            const { DOMAIN, SUB_DOMAIN, endpoints } = Constants.apiConfig;
            // const DOMAIN = "http://localhost:7000/";
            const saveAccountUrl = `${DOMAIN}${SUB_DOMAIN}${endpoints.SAVE_ACCOUNT}`;
            if (side !== "left" && side !== "right") {
                failureModal("Invalid side");
                return;
            }
            const body =
                code && side
                    ? {
                        address: address,
                        code,
                        isLeft: side === "left" ? true : false,
                    }
                    : {
                        address: address,
                    };
            const optionSaveAccount = {
                method: "POST",
                body: JSON.stringify(body),
            };
            console.log("Save Account URL", saveAccountUrl);
            console.log("Save Account Option", optionSaveAccount);
            fetch(saveAccountUrl, optionSaveAccount)
                .then((res) => res.json())
                .then((result) => {
                    console.log("Data", result);
                    if (result.code === 200) {
                        console.log("Account Data", result.data);
                        successModal("Add reference success");
                    } else {
                        failureModal("Error", result?.data?.error);
                    }
                    setOpen(false);
                })
                .catch((err) => {
                    failureModal("Error", "Something went wrong while saving account");
                    console.error("Error", err);
                    setOpen(false);
                });
        }
    };
    return (
        <Modal
            style={{ textAlign: "center" }}
            visible={open}
            okButtonProps={{ style: { display: "none" } }} // confirmLoading={confirmLoading}
            cancelButtonProps={{ style: { display: "none" } }}
        >
            <h1
                style={{
                    marginTop: "10px",
                    marginBottom: "10px",
                    fontWeight: "bold",
                }}
            >
                Staking
            </h1>
            <div className={styles.optionMain}>
                <div>
                    <Button
                        onClick={handleCreateLinkClick}
                        loading={isLoadingLink}
                        icon={
                            <PlusOutlined
                                className={styles.iconPl}
                                style={{
                                    textAlign: "center",
                                }}
                            />
                        }
                        className={styles.plusBtn}
                        style={{
                            borderRadius: "8px",
                            border: "2px solid #27aae1",
                        }}
                    ></Button>

                    <span style={{ marginLeft: "10px" }}>Create new binary tree</span>
                </div>
                <div>
                    <Button
                        onClick={() => setShowInput((showInput) => !showInput)}
                        icon={
                            <PlusOutlined
                                className={styles.iconPl}
                                style={{
                                    textAlign: "center",
                                }}
                            />
                        }
                        className={styles.plusBtn}
                        style={{
                            borderRadius: "8px",
                            border: "2px solid #27aae1",
                        }}
                    ></Button>
                    <span style={{ marginLeft: "10px" }}>Add referral link</span>
                </div>
                {showInput && <Input onChange={(e) => setReferralLink(e.target.value)} onPressEnter={handleAddReferralClick} placeholder="Add link referral"></Input>}
                <div>
                    <Button
                        style={{
                            borderRadius: "8px",
                            border: "2px solid #27aae1",
                        }}
                        className={styles.couBtn}
                        icon={
                            <RightOutlined
                                className={styles.iconPl}
                                style={{
                                    textAlign: "center",
                                }}
                            />
                        }
                        onClick={() => setOpen(false)}
                    >
                        Continue
                    </Button>
                </div>
            </div>
        </Modal>
    )
}

export default ModalReferral