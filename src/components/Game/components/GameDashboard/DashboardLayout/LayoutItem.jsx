import React from "react";
import PropTypes from "prop-types";
import clsx from "clsx";
import styles from "../../../styles.module.css";
import { Button } from "antd";
import { useState } from "react";
import { useMoralis, useWeb3ExecuteFunction } from "react-moralis";
import constants from "constant";
import { checkWalletConnection } from "helpers/auth";
import isBlackListAddress from "helpers/checkBlacklist";
import ModalReferral from "./ModalReferral";
const LayoutItem = ({ contract, item, type, image }) => {
  const { abiStaking, addrStaking, abiCollection, addrCollection } = contract;
  const { Moralis, account, authenticate, isAuthenticated } = useMoralis();
  const serverURL = process.env.REACT_APP_MORALIS_SERVER_URL;
  const appId = process.env.REACT_APP_MORALIS_APPLICATION_ID;
  Moralis.initialize(appId);
  Moralis.serverURL = serverURL;
  const contractProcessor = useWeb3ExecuteFunction();
  const [isLoading, setIsLoading] = useState(false);
  const [showModal, setShowModal] = useState(false);
  async function updateRewardRefs(event) {
    console.log(`Update reward for ${event.owner}`);
    const Profile = Moralis.Object.extend("profile");
    const query = new Moralis.Query(Profile);
    query.equalTo("address", event.owner);
    const profile = await query.first();
    const rewardPolicy = constants.rewardPolicy;
    if (profile?.attributes?.refs) {
      let refs = JSON.parse(profile.attributes.refs);
      let price = parseInt(event.price);
      // reverse iterate refs
      for (let index = refs.length - 1; index >= 0; index--) {
        const el = refs[index];
        const queryRef = new Moralis.Query("profile");
        queryRef.equalTo("address", el);
        let refInfo = await queryRef.first();
        let reward;
        if (refInfo) {
          if (
            rewardPolicy?.custom &&
            Object.keys(rewardPolicy.custom).length > 0 &&
            Object.keys(rewardPolicy.custom).includes(
              `F_${refs.length - index}`
            )
          ) {
            const rewardPercent =
              rewardPolicy?.custom?.[`F_${refs.length - index}`] ??
              rewardPolicy?.default;
            reward = rewardPercent * price;
          } else {
            reward = rewardPolicy.default * price;
          }

          refInfo.set(
            "rewards",
            refInfo.attributes.rewards
              ? refInfo.attributes.rewards + reward
              : reward
          );
          refInfo.set(
            "commission",
            refInfo.attributes.commission
              ? refInfo.attributes.commission + reward
              : reward
          );
          refInfo.save(null, { useMasterKey: true });
        }
      }
    }
    // await Moralis.Cloud.run("updateRewards", { event: event });
  }

  // const saveStakingInfo = async () => {
  //   const Staking = Moralis.Object.extend("Staking");
  //   const query = new Moralis.Query(Staking);
  //   query.equalTo("tokenId", item.tokenId);
  //   query.equalTo("staker", account);
  //   query.equalTo("addressNFT", addrCollection);
  //   query.equalTo("addressStaking", addrStaking);
  //   const result = await query.first();
  //   if (!result) {
  //     const staking = new Staking();
  //     staking.set("tokenId", item.tokenId);
  //     staking.set("staker", account);
  //     staking.set("addressNFT", addrCollection);
  //     staking.set("addressStaking", addrStaking);
  //     staking.set("image", image);
  //     staking.set("type", type);
  //     staking.set("unstake", false);
  //     staking.set("stakeTime", new Date());
  //     staking.set("name", item.title);
  //     staking.set("description", item.description);
  //     await staking.save();
  //   } else {
  //     staking.set("stakeTime", new Date());
  //     result.set("unstake", false);
  //     await result.save();
  //   }
  //   console.log("Save success");
  //   setIsLoading(false);
  // };
  async function staking() {
    console.log("Staking nft on blockchain");
    const ops = {
      contractAddress: addrStaking,
      functionName: "stake",
      abi: abiStaking,
      params: {
        _tokenIds: [item.tokenId],
      },
    };
    console.log(ops);
    await contractProcessor.fetch({
      params: ops,
      onSuccess: async () => {
        console.log("Staking success");

        await updateRewardRefs({
          owner: account,
          price: item.price,
        });
        // await saveStakingInfo();
        window.location.reload();
      },
      onError: (error) => {
        setIsLoading(false);
        console.log("Staking failed");
        return new Promise((resolve, reject) => reject(error));
      },
    });
  }

  async function approve() {
    console.log("Approve on blockchain");
    const ops = {
      contractAddress: addrCollection,
      functionName: "approve",
      abi: abiCollection,
      params: {
        to: addrStaking,
        tokenId: item?.tokenId,
      },
    };
    console.log(ops);
    await contractProcessor.fetch({
      params: ops,
      onSuccess: async () => {
        console.log("Approve success");
        await staking();
      },
      onError: (error) => {
        setIsLoading(false);
        console.log("Approve failed");
        return new Promise((resolve, reject) => reject(error));
      },
    });
  }

  async function handleStakingClicked() {
    if (!isBlackListAddress(account)) {
      setIsLoading(true);
      setShowModal(true);
      // await checkWalletConnection(isAuthenticated, authenticate, approve);
    }
  }
  return (
    <div className={clsx([styles.layoutItem, styles[type]])}>
      <span className={styles.code}>{item.code}</span>
      <span className={styles.type}>{item.code}</span>
      <img alt="" src={image} />

      <div className={styles.layoutItemRight}>
        <div></div>
        <p className={styles.title}>{item.title}</p>
        <p className={styles.description}>{item.description}</p>
        {/* <div
          className={clsx("input-text")}
          style={{ marginBottom: 5, marginTop: "auto" }}
        >
          10
        </div> */}
        {!isBlackListAddress(account) && !item?.isStakedBefore && (
          <Button
            style={{ marginBottom: 5 }}
            className={styles.startStackingBtn}
            loading={isLoading}
            onClick={() => handleStakingClicked()}
            block
          >
            Start Staking
          </Button>
        )}

        {/* <Button className={styles.sellOnMpBtn} block>
          Sell on Marketplace
        </Button> */}{
          showModal && <ModalReferral />
        }
      </div>
    </div>
  );
};

LayoutItem.propTypes = {
  type: PropTypes.oneOf(["ssr", "sr", "r"]),
};

export default LayoutItem;
