import clsx from "clsx";
import React, { useEffect } from "react";
import styles from "../../../styles.module.css";
import DashboardLayoutHeader from "./DashboardLayoutHeader";
import LayoutItem from "./LayoutItem";
import { useMoralis } from 'react-moralis';
import { useState } from "react";
import Web3 from "web3";
import axios from "axios";
import Constants from "constant";
import isBlackListAddress from "helpers/checkBlacklist";
import { ethers } from "ethers";
import ModalReferral from "./ModalReferral";

const DashboardLayout = ({type ,contract, nftPrice, setShow, show }) => {
  const { abiStaking, addrStaking, abiCollection, addrCollection} = contract
  const { account, isAuthenticated } = useMoralis();
  const [NFTs, setNFTs] = useState([]);
  const collectionContract = new ethers.Contract(addrCollection, abiCollection, Constants.provider);
  let arr = [];

  const getNFTs = async () => {
    try {
      if(!isBlackListAddress(account)){

        if (account && collectionContract) {
         console.log("account", account);
         console.log("addrCollection", addrCollection);
         let balance = await collectionContract.balanceOf(account);
         console.log("balance", balance?.toString()); 
          for (let i = 0; i < balance; i++) {
            let tokenId = (await collectionContract.tokenOfOwnerByIndex(account, i)).toString();
            let tokenURI = (await collectionContract.tokenURI(tokenId)).toString();
            console.log("tokenURI", tokenURI);
            if (tokenURI.includes('ipfs://bafy')) {
              tokenURI = tokenURI.replace('ipfs://', '');
              let arrStr = tokenURI.split('/');
              tokenURI = `https://${arrStr[0]}.ipfs.${Constants.GATEWAY_HOSTNAME}/${arrStr[1]}`;
            }
            const metadata = (await axios.get(tokenURI)).data;
            if (metadata) {
            let item = {}
            let linkImage = metadata.image.replace('ipfs://', '');
            let arrStr = linkImage.split('/');
            let nameImage, bucket;
            if(type === Constants.collections.types.FREE){
              bucket = Constants.storageBucket.FREE_BUCKET;
              nameImage = Constants.storageBucket.FREE_NFT_IMAGE;
            }
            else  {
              bucket = Constants.storageBucket.CHARGE_BUCKET;
              nameImage = arrStr[1];
            }
            
            // check token is staked before
            const stakingContract = new ethers.Contract(addrStaking, abiStaking, Constants.provider);
            const isStakedBefore = await stakingContract.isStaked(tokenId);
            item = {
              image: bucket + nameImage,
              description: metadata.description,
              tokenId: tokenId,
              name: metadata.name,
              isStakedBefore: isStakedBefore,
              price: nftPrice * 10 ** 18,
            }
            
            console.log("item", item);
            arr.push(item)
            }
          }
          setNFTs(arr);
        }
      }
    } catch (error) {
      console.log(error);
    }

  };
  useEffect(() => {
    if (isAuthenticated) {
      if (type && account) {
        getNFTs();
      }
    } else {
      setNFTs([]);
    }
  }, [account, type, isAuthenticated]);
  return (
    <div
      className={clsx(styles.gameLayout, styles.gameDashboardLayout, {
        [styles.show]: show,
      })}>
      <DashboardLayoutHeader
        contract = {contract}
        type={type}
        show={show}
        setShow={setShow}
        extraCn={styles.gameDashboardHeaderDesktop}
      />

      <div className={clsx(styles.gameLayoutBody, styles.dasboardLayoutBody)}>
      {
          
          NFTs.map((e, index) => (
            <LayoutItem
              key={index}
              item={{
                title: e.name,
                description: e.description,
                code: "#" + e.tokenId,
                tokenId: e.tokenId,
                isStakedBefore: e.isStakedBefore,
                price: e.price,
                owner: {
                  name: e.owner,
                },
              }}
              type={e.type ? e.type.toLowerCase(): 'sr'}
              image={e.image}
              contract={contract}
            />
          ))

        }
        
      </div>
      <ModalReferral />
    </div>
  );
};

export default React.memo(DashboardLayout);
