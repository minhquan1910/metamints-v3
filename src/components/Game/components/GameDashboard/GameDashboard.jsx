import Constants from "constant";
import React, { useState } from "react";
import DashboardLayout from "./DashboardLayout";
import GameDashboardContent from "./GameDashboardContent";

const GameDashboard = ({type}) => {
  const [show, setShow] = useState(false);
  let abiStaking, addrStaking, abiCollection, addrCollection, nftPrice;
  switch (type) {
    case Constants.collections.types.FREE:
      abiStaking = JSON.parse(Constants.contracts.STAKING_FREE_ABI);
      addrStaking = Constants.contracts.STAKING_FREE_ADDRESS;
      abiCollection = JSON.parse(Constants.contracts.NFT_COLLECTION_ABI);
      addrCollection = Constants.contracts.NFT_COLLECTION_ADDRESS;
      nftPrice = 0;
      break;
    case Constants.collections.types.CHARGE:
      abiStaking = JSON.parse(Constants.contracts.STAKING_NFT_ABI);
      addrStaking = Constants.contracts.STAKING_NFT_ADDRESS;
      abiCollection = JSON.parse(Constants.contracts.NFT_COLLECTION_CHARGE_ABI);
      addrCollection = Constants.contracts.NFT_COLLECTION_CHARGE_ADDRESS;
      nftPrice = Constants.NFT_PRICE;
      break;
    default:
      abiStaking = JSON.parse(Constants.contracts.STAKING_FREE_ABI);
      addrStaking = Constants.contracts.STAKING_FREE_ADDRESS;
      abiCollection = JSON.parse(Constants.contracts.NFT_COLLECTION_ABI);
      addrCollection = Constants.contracts.NFT_COLLECTION_ADDRESS;
      nftPrice = 0;
      break;
  }
  let contract = {
    abiStaking,
    addrStaking,
    abiCollection,
    addrCollection,
  }
  return (
    <>
      <GameDashboardContent type={type}  contract={contract} show={show} setShow={setShow} />

      <DashboardLayout type={type} contract={contract} nftPrice = {nftPrice} show={show} setShow={setShow} />
    </>
  );
};

export default React.memo(GameDashboard);
