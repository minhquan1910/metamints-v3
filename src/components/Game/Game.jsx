import React, { useCallback } from 'react';
import { useParams } from 'react-router-dom';
import GameDashboard from './components/GameDashboard';
import styles from './styles.module.css';

const Game = () => {
  const { type } = useParams();

  // const withLayout = useMemo(
  //   () => type === 'buy-properties' || type === 'dashboard',
  //   [type]
  // );

  return <div className={styles.gameContainer}>
    <GameDashboard type={type} />
  </div>;
};

export default Game;
