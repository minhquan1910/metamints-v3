import { Col, Pagination, Row } from "antd";
import Collection from "components/Collection";
import Constants from "constant";
import { getCollectionsByChain } from "helpers/collection";
import React from "react";
import { useMoralis } from "react-moralis";
import Cardbox from "./Cardbox";
import styless from "./Explores.module.css";

const Explores = () => {
  const { chainId } = useMoralis();
  const NFTCollections = getCollectionsByChain(chainId);

  function itemRender(current, type, originalElement) {
    if (type === "prev") {
      return null;
    }
    if (type === "next") {
      return null;
    }
    return originalElement;
  }
  return (
    <div
      className={styless.wrapper}
      onScroll={(e) => console.log(e.target.scrollTop)}
    >
      <Row
        gutter={[10, 16]}
        justify={NFTCollections?.length < 3 ? "center" : "start"}
      >
        {NFTCollections &&
          NFTCollections.map((nftCollection, index) => (
            <Col
              span={24}
              sm={{ span: 12 }}
              xl={{ span: 8 }}
              key={index}
            >
              <Cardbox
                item={{
                  ...nftCollection,
                }}
              />
            </Col>
            // </Link>
          ))}
        <Col span={24}>
          <Row justify="center" style={{ marginTop: "24px" }}>
            <Pagination
              itemRender={itemRender}
              className={styless.pagination}
              defaultCurrent={1}
              total={50}
            />
          </Row>
        </Col>
      </Row>
    </div>
  );
};

export default Explores;
