import Text from "antd/lib/typography/Text";
import { Button, Modal } from "antd";
import { useState } from "react";
import { connectors } from "../Account/config";
import { useMoralis } from "react-moralis";
import { useHistory, useParams } from "react-router";
import Constants from "constant";
import { failureModal, successModal } from "helpers/modals";
const styles2 = {
    account: {
        height: "42px",
        padding: "0 15px",
        display: "flex",
        justifyContent: "center",
        alignItems: "center",
        width: "fit-content",
        borderRadius: "12px",
        border: "solid 2px white",
        cursor: "pointer",
    },
    text: {
        color: "var(--theme-color)",
    },
    connector: {
        alignItems: "center",
        display: "flex",
        flexDirection: "column",
        height: "auto",
        justifyContent: "center",
        marginLeft: "auto",
        marginRight: "auto",
        padding: "20px 5px",
        cursor: "pointer",
    },
    icon: {
        alignSelf: "center",
        fill: "rgb(40, 13, 95)",
        flexShrink: "0",
        marginBottom: "8px",
        height: "30px",
    },
};

const ConnectModal = () => {
    console.log("ConnectModal");
    const { side, code } = useParams();
    console.log("side", side);
    console.log("code", code);
    const { isAuthenticated, account, authenticate } = useMoralis();
    const [isAuthModalVisible, setIsAuthModalVisible] = useState(true);
    const addRef = (address) => {

    }
    return (
        <Modal
            visible={isAuthModalVisible}
            footer={null}
            onCancel={() => setIsAuthModalVisible(false)}
            bodyStyle={{
                padding: "15px",
                fontSize: "17px",
                fontWeight: "500",
            }}
            style={{ fontSize: "16px", fontWeight: "500" }}
            width="340px"
        >
            <div
                style={{
                    padding: "10px",
                    display: "flex",
                    justifyContent: "center",
                    fontWeight: "700",
                    fontSize: "20px",
                }}
            >
                Connect Wallet
            </div>
            <div style={{ display: "grid", gridTemplateColumns: "1fr 1fr" }}>
                {connectors.map(({ title, icon, connectorId }, key) => (
                    <div
                        style={styles2.connector}
                        key={key}
                        onClick={async () => {
                            try {
                                await authenticate({
                                    provider: connectorId, // required
                                })
                                window.localStorage.setItem("connectorId", connectorId);
                                console.log("user", account);
                                if (account) {
                                    addRef(account);
                                }
                                setIsAuthModalVisible(false);
                            } catch (e) {
                                console.error(e);
                                failureModal("Error", "Something went wrong while connecting wallet");
                            }
                        }}
                    >
                        <img src={icon} alt={title} style={styles2.icon} />
                        <Text style={{ fontSize: "14px" }}>{title}</Text>
                    </div>
                ))}
            </div>
        </Modal>
    );
}
export default ConnectModal;