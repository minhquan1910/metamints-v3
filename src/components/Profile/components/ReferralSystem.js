import { PlusOutlined, RightOutlined } from "@ant-design/icons";
import { Card, Col, Row, Typography, Tooltip, Grid, Spin, Button } from "antd";
import clsx from "clsx";
import { CopyIcon } from "components/Icons";
import styles from "../styles.module.css";
import "./Refferal.css";
import Tree from "react-d3-tree";
import { useMoralis } from "react-moralis";
import { useEffect, useState } from "react";
import { Link, useParams } from "react-router-dom";
import Constants from "constant";
import BinaryTree from "helpers/binaryTree";
import { useCenteredTree } from "./helpers";
import { isEmpty } from "lodash";
import LoadingIndicator from "components/LoadingIndicator/LoadingIndicator";
import { failureModal } from "helpers/modals";
import { checkWalletConnection } from "helpers/auth";
let totalSystemRef = 0;
const { useBreakpoint } = Grid;
const ReferralSystem = ({ toggleReferral }) => {
  const [data, setData] = useState({});
  const { xs } = useBreakpoint();
  const [commission, setCommission] = useState(0);
  const [totalSystemValue, setTotalSystemValue] = useState(0);
  const { account, isAuthenticated, authenticate } = useMoralis();
  const [isLoadingTree, setIsLoadingTree] = useState(false);
  const [isLoadingLink, setIsLoadingLink] = useState(false);
  const [binaryData, setBinaryData] = useState({});
  const [isRoot, setIsRoot] = useState(false);
  const [translate, containerRef] = useCenteredTree();
  const renderRectSvgNode = ({ nodeDatum, toggleNode }) => (
    <g>
      <circle r="15" onClick={toggleNode} />
      {/* <circle width="20" height="20" x="-10" onClick={toggleNode} /> */}
      <text fill="black" strokeWidth="1" x="20" fontSize="12px">
        {nodeDatum.name}
      </text>
      {/* {nodeDatum.attributes?.department && (
        <text fill="black" x="20" dy="20" strokeWidth="1" fontSize="12px">
          Department: {nodeDatum.attributes?.department}
        </text>
      )} */}
    </g>
  );
  const containerStyles = {
    width: "100vw",
    height: "100vh",
    marginTop: "-310px",
  };
  // check exist account
  useEffect(() => {
    if (isAuthenticated && account) {
      const { DOMAIN, SUB_DOMAIN, endpoints } = Constants.apiConfig;
      // const DOMAIN = "http://localhost:7000/";
      const getAccountUrl = `${DOMAIN}${SUB_DOMAIN}${endpoints.GET_ACCOUNT}`;
      const body = {
        address: account,
      };
      const optionGetAccount = {
        method: "POST",
        body: JSON.stringify(body),
      };
      console.log("Get Account URL", getAccountUrl);
      console.log("Get Account Option", optionGetAccount);
      fetch(getAccountUrl, optionGetAccount)
        .then((res) => res.json())
        .then((result) => {
          console.log("Data", result);
          if (result?.code === 200) {
            console.log("Account Data", result?.data);
            if (!isEmpty(result?.data)) {
              setData({
                ...result.data,
                linkLeft: `${window.location.origin}/invite/left/${result.data.code}`,
                linkRight: `${window.location.origin}/invite/right/${result.data.code}`,
              });
              setIsRoot(false);
              if (!isEmpty(result?.data) && result.data?.isCloned) {
                setIsLoadingTree(true);
                const getTreeUrl = `${DOMAIN}${SUB_DOMAIN}${endpoints.GET_TREE}`;
                const optionGetTree = {
                  method: "POST",
                  body: JSON.stringify({
                    address: account,
                    contract: result?.data?.contract,
                    contractOwner: result?.data?.contractOwner,
                    isCloned: result?.data?.isCloned,
                  }),
                };
                console.log("Get Tree URL", getTreeUrl);
                console.log("Get Tree Option", optionGetTree);
                fetch(getTreeUrl, optionGetTree)
                  .then((res) => res.json())
                  .then((resData) => {
                    console.log("Data", resData);
                    if (resData.code === 200) {
                      console.log("Tree Data", resData.data);
                      const binaryTree = new BinaryTree(resData.data?.slice(1));
                      const root = binaryTree.insertLevelOrder(0);
                      if (!isEmpty(root)) {
                        const binaryDataSet = root.parseData()
                        console.log("Root", root);
                        console.log("Binary Data Set", binaryDataSet);
                        setBinaryData(binaryDataSet);
                      }
                    } else {
                      failureModal("Error", resData?.data?.error);
                    }
                    setIsLoadingTree(false);
                  })
                  .catch((err) => {
                    console.error("Error", err);
                    failureModal("Error", "Something went wrong when getting tree");
                    setIsLoadingTree(false);
                  });
              }
            }
            else{
              setData({});
              setIsRoot(true);
            }
          } else {
            failureModal("Error", result?.data?.error);
          }
          // setIsLoadingLink(false);
        })
        .catch((err) => {
          failureModal("Error", "Something went wrong while checking account");
          console.error("Error", err);
          setIsLoadingLink(false);
        });
    }
  }, [account, isAuthenticated]);

  function getTotalSystem(array) {
    array.forEach((element) => {
      // console.log(element);
      if (!element.commission) {
        element.commission = 0;
      }
      totalSystemRef = totalSystemRef + element.commission;
      if (element.children.length > 0) {
        getTotalSystem(element.children);
      }
    });
  }

  const handleCopy = (text) => {
    navigator.clipboard.writeText(text);
  };
  const renderValue = (number) => {
    if (number > 0) {
      return xs ? number.toFixed(7) : number.toFixed(10);
    } else {
      return number;
    }
  };

  const createLink = async () => {
    setIsLoadingLink(true);
    const { DOMAIN, SUB_DOMAIN, endpoints } = Constants.apiConfig;
    // const DOMAIN = "http://localhost:7000/";
    const saveAccountUrl = `${DOMAIN}${SUB_DOMAIN}${endpoints.SAVE_ACCOUNT}`;
    const body = {
      address: account,
    };
    const optionSaveAccount = {
      method: "POST",
      body: JSON.stringify(body),
    };
    console.log("Save Account URL", saveAccountUrl);
    console.log("Save Account Option", optionSaveAccount);
    fetch(saveAccountUrl, optionSaveAccount)
      .then((res) => res.json())
      .then((result) => {
        console.log("Data", result);
        if (result.code === 200) {
          console.log("Account Data in create link", result.data);
          setData({
            ...result.data,
            linkLeft: `${window.location.origin}/invite/left/${result.data.code}`,
            linkRight: `${window.location.origin}/invite/right/${result.data.code}`,
          });
        } else {
          failureModal("Error", result?.data?.error);
        }
        setIsLoadingLink(false);
      })
      .catch((err) => {
        failureModal("Error", "Something went wrong while saving account");
        console.error("Error", err);
        setIsLoadingLink(false);
      });
  };
  const handleCreateLinkClick = async () => {
    await checkWalletConnection(isAuthenticated, authenticate, createLink);
  };
  // const renderNode = (children) =>
  //   children?.map((child) => (
  //     <TreeNode
  //       selectable={false}
  //       key={child.address}
  //       title={
  //         <div
  //           className={clsx(styles.box, styles.nodeBox)}
  //           onClick={() => handleCopy(child.address)}
  //         >
  //           <Tooltip title="Copied" trigger="click" placement="left">
  //             <div className={styles.nodeLeft}>
  //               <Typography.Text strong>{compactAddress(child?.address)}</Typography.Text>
  //             </div>
  //           </Tooltip>
  //           <div className={styles.nodeRight}>
  //             Commission: {renderValue(child.commission)} BUSD
  //           </div>
  //         </div>
  //       }
  //     >
  //       {child?.children && renderNode(child)}
  //     </TreeNode>
  //   ));

  return (
    <>
      <div style={{ display: "flex", justifyContent: "center" }}>
        <Card className={styles.card}>
          <Link to="/profile">
            <RightOutlined className={styles.btnBack} />
          </Link>
          <Row gutter={[16, 16]}>
            <div className={styles.referalHeader} style={{ padding: "0 8px" }}>
              <div></div>

              <div
                className={styles.referralTitle}
                style={!isRoot ? { paddingLeft: "0px" } : null}
              >
                Referral System
              </div>
              <div>
                {isRoot && (
                  <Button
                    onClick={handleCreateLinkClick}
                    icon={
                      <PlusOutlined
                        className={styles.iconPl}
                        style={{
                          textAlign: "center",
                        }}
                      />
                    }
                    className={styles.plusBtn}
                    style={{
                      borderRadius: "8px",
                      border: "2px solid #27aae1",
                    }}
                  ></Button>
                )}
              </div>
            </div>
            {/* <Col span={24}>
              <div className={clsx(styles.addressBox, styles.box)}>
                <Typography.Text
                  strong
                  style={{
                    maxWidth: "100%",
                  }}
                  ellipsis={{
                    tooltip: data?.address,
                  }}
                >
                  {data?.address}
                </Typography.Text>
                <Tooltip title="Copied" trigger="click" placement="top">
                  <span className={styles.iconCopy}>
                    <CopyIcon
                      onClick={() => handleCopy(data?.address)}
                      style={{ color: "#fff", fontSize: 12 }}
                    />
                  </span>
                </Tooltip>
              </div>
            </Col> */}

            <Col span={24}>
              <Spin spinning={isLoadingLink}>
                <div className={clsx(styles.addressBox, styles.box)}>
                  <Typography.Text
                    strong
                    style={{
                      maxWidth: "100%",
                    }}
                    ellipsis={{
                      tooltip: data?.linkLeft,
                    }}
                  >
                    {data?.linkLeft}
                  </Typography.Text>
                  <Tooltip title="Copied" trigger="click" placement="top">
                    <span className={styles.iconCopy}>
                      <CopyIcon
                        onClick={() => handleCopy(data?.linkLeft)}
                        style={{ color: "#fff", fontSize: 12 }}
                      />
                    </span>
                  </Tooltip>
                </div>
              </Spin>
            </Col>
            <Col span={24}>
              <Spin spinning={isLoadingLink}>
                <div className={clsx(styles.addressBox, styles.box)}>
                  <Typography.Text
                    strong
                    style={{
                      maxWidth: "100%",
                    }}
                    ellipsis={{
                      tooltip: data?.linkRight,
                    }}
                  >
                    {data?.linkRight}
                  </Typography.Text>
                  <Tooltip title="Copied" trigger="click" placement="top">
                    <span className={styles.iconCopy}>
                      <CopyIcon
                        onClick={() => handleCopy(data?.linkRight)}
                        style={{ color: "#fff", fontSize: 12 }}
                      />
                    </span>
                  </Tooltip>
                </div>
              </Spin>
            </Col>
            <Col span={24}>
              <Spin spinning={isLoadingTree}>
                <div className={clsx(styles.infoTotalBox, styles.box)}>
                  <Row gutter={4} style={{ width: "100%" }}>
                    <Col span={12}>
                      <span>Total System:</span>
                    </Col>
                    <Col span={12}>
                      <span>{totalSystemValue} BUSD</span>
                    </Col>
                  </Row>
                </div>
              </Spin>
            </Col>
            <Col span={24}>
              {/* <div className={clsx(styles.infoCommissionlBox, styles.box)}> */}

              <div style={{ display: "flex" }}>
                <div className={clsx(styles.infoCommissionlBox, styles.box)}>
                  <Col flex={3}>
                    <span>Your Commission:</span>
                  </Col>
                  <Col flex={2}>
                    <span>{commission.toFixed(2)} BUSD</span>
                  </Col>
                </div>

                <Col span={6} style={{ paddingRight: "0px" }}>
                  <Button
                    style={{
                      background: "#fff",
                      borderRadius: 8,
                      border: "2px solid #8578ff",
                      width: "100%",
                      height: "100%",
                      color: "#8578ff",
                      fontWeight: "bold",
                    }}
                    className={styles["claim-btn"]}
                  >
                    Claim
                  </Button>
                </Col>
              </div>
            </Col>
            {/* {isLoading ? (
              <LoadingIndicator />
            ) : (
              <Col span={24}>
                <div className={styles.tree}>
                  <Tree
                    showLine={{ showLeafIcon: false }}
                    defaultExpandAll={true}
                    switcherIcon={null}
                    className={styles.referralNode}
                  >
                    {children && renderNode(children)}
                  </Tree>
                </div>
              </Col>
            )} */}
            {isLoadingTree ? (
              <LoadingIndicator />
            ) : (
              !isEmpty(binaryData) &&
              data?.isCloned && (
                <div
                  id="treeWrapper"
                  style={containerStyles}
                  ref={containerRef}
                >
                  <Tree
                    rootNodeClassName="node__root"
                    branchNodeClassName="node__branch"
                    leafNodeClassName="node__leaf"
                    translate={translate}
                    // renderCustomNodeElement={renderRectSvgNode}
                    // renderCustomNodeElement={renderRectSvgNode}
                    data={binaryData}
                    orientation="vertical"
                    pathFunc="straight"
                    nodeSize={{ x: 100, y: 100 }}
                  />
                </div>
              )
            )}
          </Row>
        </Card>
      </div>
    </>
  );
};

export default ReferralSystem;
