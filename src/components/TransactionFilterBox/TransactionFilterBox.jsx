import {
  FilterOutlined,
  LeftOutlined,
  DownOutlined,
  UpOutlined,
} from "@ant-design/icons";
import { Divider, Form, Layout, Typography, Grid } from "antd";
import React, { useState, useEffect } from "react";
import CategoryFilter from "./CategoryFilter";
import CollectionFilter from "./CollectionFilter";
import PriceFilter from "./PriceFilter";
import StatusFilter from "./StatusFilter";
import styless from "./Filter.module.css";
import clsx from "clsx";

const { Sider } = Layout;
const { useBreakpoint } = Grid;

const TransactionFilterBox = () => {
  const [form] = Form.useForm();
  const [collapsed, setCollapsed] = useState(false);
  const [visibleContent, setVisibleContent] = useState(false);

  const screens = useBreakpoint();

  useEffect(() => {
    if (Object.keys(screens).length !== 0) {
      setCollapsed(!screens.md);
      setVisibleContent(!screens.md);
    }
  }, [screens]);

  const toggle = () => {
    setCollapsed((collapsed) => !collapsed);

    setTimeout(
      () => {
        setVisibleContent(!collapsed);
      },
      !collapsed ? 0 : 70
    );
  };
  return (
    <div className={`${collapsed && styless.wrapperCollapsed}`}>
      <Sider
        trigger={null}
        collapsible
        collapsed={collapsed}
        theme="light"
        collapsedWidth={60}
        width={300}
        className={clsx(styless.filterSider, "filter-container")}
      >
        {!visibleContent ? (
          <div className={styless.filterTitle}>
            <FilterOutlined />
            <Typography.Text strong>Filter</Typography.Text>
            {!screens.md ? (
              <DownOutlined onClick={toggle} />
            ) : (
              <LeftOutlined onClick={toggle} />
            )}
          </div>
        ) : (
          <div
            className={!screens.md ? styless.filterTitle : styless.filterIcon}
            onClick={toggle}
          >
            <FilterOutlined />
            {!screens.md && <Typography.Text strong>Filter</Typography.Text>}
            {!screens.md && <UpOutlined />}
          </div>
        )}
        {!visibleContent && (
          <Form form={form}>
            <Divider key = {1} className={styless.divider} />
            <StatusFilter key={2} form={form} />
            <Divider key={3} className={styless.divider} />
            <PriceFilter key={4} form={form} />
            <Divider key={5} className={styless.divider} />
            <CollectionFilter key={6} form={form} />
            <Divider key={7} className={styless.divider} />
            <CategoryFilter key={8} form={form} />
          </Form>
        )}
      </Sider>
    </div>
  );
};

export default TransactionFilterBox;
