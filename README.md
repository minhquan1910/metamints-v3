# 🚀 Quick Start

📄 Clone project:
```sh
https://github.com/Metapolis21/metamints-v3.git
```
💿 Install all dependencies:
```sh
yarn install 
```
✏ Rename `.env.example` to `.env` in the main folder and add API
REACT_APP_MORALIS_APPLICATION_ID = 
REACT_APP_MORALIS_SERVER_URL = 

✏ Automatically Deploy Status: [![Netlify Status](https://api.netlify.com/api/v1/badges/d9eedca5-ed07-4ff5-b84f-5ab67978395b/deploy-status)](https://app.netlify.com/sites/metamint-v3/deploys)
```
🚴‍♂️ Run your App:
```sh
yarn start
```



